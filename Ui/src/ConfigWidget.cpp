/**********************************************************
* file: ConfigWidget.h
* synopsis: API for ConfigWidget class
* author: Vladyslav Podilnyk
**********************************************************/

#include <sstream>
#include <iomanip>
#include <stdexcept>

#include <QHBoxLayout>
#include <QFormLayout>
#include <QSpacerItem>
#include <QErrorMessage>

#include "ConfigValidator.h"
#include "ConfigWidget.h"


ConfigWidget::ConfigWidget(QWidget *parent)
    : QWidget                       {parent}
    , writeBackPolicyCheck          {nullptr}
    , writeOnAllocPolicyCheck       {nullptr}
    , writeThroughPolicyCheck       {nullptr}
    , writeAroundPolicyCheck        {nullptr}
    , lruCacheCheck                 {nullptr}
    , fifoCacheCheck                {nullptr}
    , storeInstrCheck               {nullptr}
    , loadInstrCheck                {nullptr}
    , resetCacheConfigButton        {nullptr}
    , submitCacheConfigButton       {nullptr}
    , generateInstructionsButton    {nullptr}
    , nextStepButton                {nullptr}
    , stepByStepButton              {nullptr}
    , forwardInstrButton            {nullptr}
    , forwardAllButton              {nullptr}
    , resetInstrExec                {nullptr}
    , controllerInfoBox             {nullptr}
    , cacheSizeEdit                 {nullptr}
    , memorySizeEdit                {nullptr}
    , offsetBitsEdit                {nullptr}
    , instrNumberEdit               {nullptr}
    , currentInstrEdit              {nullptr}
    , loadInstrProbability          {nullptr}
    , instrTypePanel                {nullptr}
    , mainLayout                    {new QVBoxLayout()}
    , nextStep                      {false}
    , isForward                     {false}  
    , runAllInstr                   {false}
    , isInstrQueueEmpty             {true}
    , isConfigSet                   {false}
    , instrGenerator                {new core::InstructionGen()}
{
    createWidgets();
    createLayouts();
    createConnections();

}


ConfigWidget::~ConfigWidget()
{
    delete instrGenerator;
}


void ConfigWidget::createWritePolicyRadioButtons()
{
    writeBackPolicyCheck = new QRadioButton(tr("Write Back"), this);
    writeOnAllocPolicyCheck = new QRadioButton(tr("Write On Allocate"), this);
    writeThroughPolicyCheck = new QRadioButton(tr("Write Through"), this);
    writeAroundPolicyCheck = new QRadioButton(tr("Write Around"), this);

    QButtonGroup *wpButtonGroupFirst = new QButtonGroup(this);
    wpButtonGroupFirst->addButton(writeBackPolicyCheck);
    wpButtonGroupFirst->addButton(writeThroughPolicyCheck);

    QButtonGroup *wpButtonGroupSecond = new QButtonGroup(this);
    wpButtonGroupSecond->addButton(writeOnAllocPolicyCheck);
    wpButtonGroupSecond->addButton(writeAroundPolicyCheck);
}


void ConfigWidget::createReplacementPolicyRadioButtons()
{
    lruCacheCheck = new QRadioButton(tr("LRU"), this);
    fifoCacheCheck = new QRadioButton(tr("FIFO"), this);
}


void ConfigWidget::createInstrTypeRadioButtons()
{
    storeInstrCheck = new QRadioButton(tr("Store"), this);
    loadInstrCheck = new QRadioButton(tr("Load"), this);
}


void ConfigWidget::createControlElements()
{
    resetCacheConfigButton = new QPushButton(tr("Reset"), this);
    submitCacheConfigButton = new QPushButton(tr("Submit"), this);
    generateInstructionsButton = new QPushButton(tr("Generate Instructions"), this);
    stepByStepButton = new QPushButton(tr("Step by step"), this);
    nextStepButton = new QPushButton(tr("Next Step"), this);
    forwardInstrButton = new QPushButton(tr("Forward Instruction"), this);
    forwardAllButton = new QPushButton(tr("Execute"), this);
    resetInstrExec = new QPushButton(tr("Reset Instructions Execution"), this);
    nextStepButton->setDisabled(true);
    forwardInstrButton->setDisabled(true);

    loadInstrProbability = new QDoubleSpinBox(this);
    loadInstrProbability->setRange(0, 1);
    loadInstrProbability->setSingleStep(probabilityStep);
    loadInstrProbability->setDisabled(true);
}


void ConfigWidget::createLineEdits()
{
    cacheSizeEdit = new QLineEdit(this);
    memorySizeEdit = new QLineEdit(this);
    offsetBitsEdit = new QLineEdit(this);
    instrNumberEdit = new QLineEdit(this);
    currentInstrEdit = new QLineEdit(this);

    cacheSizeEdit->setValidator(new QIntValidator);
    memorySizeEdit->setValidator(new QIntValidator);
    offsetBitsEdit->setValidator(new QIntValidator);
    instrNumberEdit->setValidator(new QIntValidator);
    currentInstrEdit->setReadOnly(true);
}


void ConfigWidget::createWritePolicyPanel()
{
    QHBoxLayout *writePolicyTopHalf = new QHBoxLayout();
    writeBackPolicyCheck->setChecked(true);
    writeOnAllocPolicyCheck->setChecked(true);
    writePolicyTopHalf->addWidget(writeBackPolicyCheck);
    writePolicyTopHalf->addWidget(writeOnAllocPolicyCheck);

    QHBoxLayout *writePolicyBottomHalf = new QHBoxLayout();
    writePolicyBottomHalf->addWidget(writeThroughPolicyCheck);
    writePolicyBottomHalf->addWidget(writeAroundPolicyCheck);

    QVBoxLayout *writePolicyLayout = new QVBoxLayout();
    writePolicyLayout->addLayout(writePolicyTopHalf);
    writePolicyLayout->addLayout(writePolicyBottomHalf);

    QGroupBox *writePolicyPanel = new QGroupBox(tr("Write Policies"));
    writePolicyPanel->setLayout(writePolicyLayout);

    mainLayout->addWidget(writePolicyPanel);
    mainLayout->addStretch();
}


void ConfigWidget::createReplacementPolicyPanel()
{
    QHBoxLayout *replacementPolicyLayout = new QHBoxLayout();
    lruCacheCheck->setChecked(true);
    replacementPolicyLayout->addWidget(lruCacheCheck);
    replacementPolicyLayout->addWidget(fifoCacheCheck);

    QGroupBox *replacementPolicyPanel = new QGroupBox(tr("Replacement Policies"));
    replacementPolicyPanel->setLayout(replacementPolicyLayout);

    mainLayout->addWidget(replacementPolicyPanel);
    mainLayout->addStretch();
}


void ConfigWidget::createCacheConfigPanel()
{
    QFormLayout *cacheMemConfig = new QFormLayout();
    cacheMemConfig->addRow(tr("Cache Size:"), cacheSizeEdit);
    cacheMemConfig->addRow(tr("Memory Size:"), memorySizeEdit);
    cacheMemConfig->addRow(tr("Offset Bits:"), offsetBitsEdit);

    QHBoxLayout *configCtrl = new QHBoxLayout();
    configCtrl->addWidget(resetCacheConfigButton);
    configCtrl->addWidget(submitCacheConfigButton);

    mainLayout->addLayout(cacheMemConfig);
    mainLayout->addLayout(configCtrl);
    mainLayout->addStretch();
}


void ConfigWidget::createInstructionForm()
{
    QFormLayout *numberOfInstrForm = new QFormLayout();
    numberOfInstrForm->addRow(tr("Number of Intsructions:"), instrNumberEdit);

    QHBoxLayout *probabilityPanel = new QHBoxLayout();
    probabilityPanel->addWidget(new QLabel("Load instruction probability:"));
    probabilityPanel->addWidget(loadInstrProbability);

    QHBoxLayout *execType = new QHBoxLayout();
    execType->addWidget(stepByStepButton);
    execType->addWidget(forwardAllButton);

    mainLayout->addLayout(numberOfInstrForm);
    mainLayout->addLayout(probabilityPanel);
    mainLayout->addLayout(execType);
    mainLayout->addWidget(generateInstructionsButton);
    mainLayout->addWidget(resetInstrExec);
    mainLayout->addStretch();
}


void ConfigWidget::createExecutionControlPanel()
{
    QHBoxLayout *execType = new QHBoxLayout();
    execType->addWidget(stepByStepButton);
    execType->addWidget(forwardAllButton);

    QHBoxLayout *instrTypeLayout = new QHBoxLayout();
    loadInstrCheck->setChecked(true);
    instrTypeLayout->addWidget(loadInstrCheck);
    instrTypeLayout->addWidget(storeInstrCheck);

    instrTypePanel = new QGroupBox(tr("Instruction Type"));
    instrTypePanel->setLayout(instrTypeLayout);
    instrTypePanel->setEnabled(false);

    QFormLayout *currInstrForm = new QFormLayout();
    currInstrForm->addRow(tr("Current Intsruction:"), currentInstrEdit);

    QHBoxLayout *ctrlButtonsGrid = new QHBoxLayout();
    ctrlButtonsGrid->addWidget(nextStepButton);
    ctrlButtonsGrid->addWidget(forwardInstrButton);

    mainLayout->addWidget(instrTypePanel);
    mainLayout->addStretch();
    mainLayout->addLayout(currInstrForm);
    mainLayout->addLayout(ctrlButtonsGrid);
    mainLayout->addStretch();
}


void ConfigWidget::createWidgets()
{
    createWritePolicyRadioButtons();
    createReplacementPolicyRadioButtons();
    createInstrTypeRadioButtons();
    createControlElements();
    createLineEdits();
}


void ConfigWidget::createLayouts()
{
    createWritePolicyPanel();
    createReplacementPolicyPanel();
    createCacheConfigPanel();
    createInstructionForm();
    createExecutionControlPanel();
    this->setLayout(mainLayout);
}


void ConfigWidget::createConnections()
{
    connect(submitCacheConfigButton, &QPushButton::clicked, this, &ConfigWidget::onSubmitButtonClick);
    connect(resetCacheConfigButton, &QPushButton::clicked, this, &ConfigWidget::onResetButtonClick);
    connect(stepByStepButton, &QPushButton::clicked, this, &ConfigWidget::onStepByStepButtonClick);
    connect(generateInstructionsButton, &QPushButton::clicked, this, &ConfigWidget::onGenerateInsructionClick);
    connect(nextStepButton, &QPushButton::clicked, this, &ConfigWidget::onNextStepButtonClick);
    connect(forwardInstrButton, &QPushButton::clicked, this, &ConfigWidget::onForwardInstrButtonClick);
    connect(forwardAllButton, &QPushButton::clicked, this, &ConfigWidget::onForwardAllButtonClick);
    connect(resetInstrExec, &QPushButton::clicked, this, &ConfigWidget::onResetInstrExecButtonClick);
}


void ConfigWidget::onSubmitButtonClick()
{
    auto firstPolicy = (writeBackPolicyCheck->isChecked()) ? core::WritePolicy::write_back
                                                           : core::WritePolicy::write_through;
    auto secondPolicy = (writeOnAllocPolicyCheck->isChecked()) ? core::WritePolicy::write_on_alloc
                                                               : core::WritePolicy::write_around;

    auto rpPolicy = (lruCacheCheck->isChecked()) ? core::ReplacementPolicy::lru
                                                 : core::ReplacementPolicy::fifo;

    size_t memorySize = memorySizeEdit->text().toUInt();
    size_t cacheSize = cacheSizeEdit->text().toUInt();
    size_t offset = offsetBitsEdit->text().toUInt();

    if (ConfigValidator::validate(memorySize, cacheSize, offset) == QValidator::State::Invalid) {
        ConfigValidator::message("Configuration is invalid.", core::ValidatorFlags::error);
        return;
    }

    core::CacheParams params = {.wr_policy = {firstPolicy, secondPolicy},
                                .rp_policy = rpPolicy,
                                .memory_size = memorySize,
                                .cache_size = cacheSize,
                                .offset = offset
                               };
    emit setParameters(params);
    submitCacheConfigButton->setDisabled(true);
    instrGenerator->setConfig(memorySize, offset);
    isConfigSet = true;
}


void ConfigWidget::onResetButtonClick()
{
    cacheSizeEdit->clear();
    memorySizeEdit->clear();
    offsetBitsEdit->clear();
    instrNumberEdit->clear();

    writeBackPolicyCheck->setChecked(true);
    writeOnAllocPolicyCheck->setChecked(true);
    lruCacheCheck->setChecked(true);
    loadInstrCheck->setChecked(true);

    submitCacheConfigButton->setDisabled(false);
    stepByStepButton->setDisabled(false);
    forwardAllButton->setDisabled(false);
    nextStepButton->setDisabled(true);
    forwardInstrButton->setDisabled(true);

    instrGenerator->reset();
    isConfigSet = false;
    emit flushSignal();
}


void ConfigWidget::onStepByStepButtonClick()
{
    forwardAllButton->setDisabled(true);
    instrTypePanel->setEnabled(true);
    nextStepButton->setDisabled(false);
    forwardInstrButton->setEnabled(true);
    runAllInstr = false;
}


std::string ConfigWidget::intToHexString(size_t number)
{
    std::stringstream stream;
    stream << "0x" << std::hex << number;
    return stream.str();
}


void ConfigWidget::onGenerateInsructionClick()
{
    if (!instrNumberEdit->text().toUInt()) {
        ConfigValidator::message("Number of instructions is not set.", core::ValidatorFlags::error);
        return;
    }

    if (!isConfigSet) {
        ConfigValidator::message("Cache configuration is not set.", core::ValidatorFlags::error);
        return;
    }

    if (!runAllInstr) {
        instrGenerator->generate(instrNumberEdit->text().toUInt(), false);
        currentInstrEdit->setText(QString::fromUtf8(intToHexString(instrGenerator->getInstr(0).instr).c_str()));
    }
    else
        instrGenerator->generate(instrNumberEdit->text().toUInt(), true, loadInstrProbability->value());
    
    isInstrQueueEmpty = false;
}


void ConfigWidget::onNextStepButtonClick()
{
    if (runAllInstr && !isInstrQueueEmpty) {
        auto instr_list = instrGenerator->getInstrList();
        isInstrQueueEmpty = true;
        emit sendInstructions(instr_list);
    }
    else if (nextStep) {
        emit makeNextStep();
        isForward = false;
    }
    else if (!isInstrQueueEmpty) {
        core::InstrInfo info;

        try {
            info = instrGenerator->getInstr();
        }
        catch (const std::out_of_range &error) {
            isInstrQueueEmpty = true;
            ConfigValidator::message("Instruction queue is empty.", core::ValidatorFlags::info);
            return;
        }

        currentInstrEdit->setText(QString::fromUtf8(intToHexString(info.instr).c_str()));
        info.type = (loadInstrCheck->isChecked()) ? core::InstrType::load : core::InstrType::store;
        emit sendInstruction(info);
        nextStep = true;
        isForward = true;
    }
    else
        ConfigValidator::message("Instruction queue is empty.", core::ValidatorFlags::info);
}


void ConfigWidget::onResetInstrExecButtonClick()
{
    instrGenerator->reset();
    forwardAllButton->setDisabled(false);
    stepByStepButton->setDisabled(false);
    instrTypePanel->setEnabled(false);
    nextStepButton->setDisabled(true);
    nextStepButton->setText("Next Step");
    forwardInstrButton->setDisabled(true);
    loadInstrProbability->setDisabled(true);
    currentInstrEdit->clear();
    runAllInstr = false;
    instrGenerator->reset();
    emit resetInstrExecSignal();
}


void ConfigWidget::onForwardAllButtonClick()
{
    stepByStepButton->setDisabled(true);
    loadInstrProbability->setDisabled(false);
    nextStepButton->setDisabled(false);
    nextStepButton->setText("Run");
    runAllInstr = true;
}


void ConfigWidget::onForwardInstrButtonClick()
{
    if (!isForward) {
        ConfigValidator::message("Cannot forward the instruction.", core::ValidatorFlags::error);
        return;
    }
    emit sendInstrForwardExec();
}


void ConfigWidget::onCacheIsNotConfigured()
{
    ConfigValidator::message("You must configure cache!", core::ValidatorFlags::error);
}
/**********************************************************
* file: MainWidget.cpp
* synopsis: Implementation for MainWidget class
* author: Vladyslav Podilnyk
**********************************************************/

#include <QHBoxLayout>
#include "MainWidget.h"


MainWidget::MainWidget(QWidget *parent)
    :QWidget {parent}
{
    createWidgets();
    createLayouts();
    createConnections();
}

void MainWidget::createWidgets()
{
    setWindowTitle (tr("CacheSim"));
    
    configWidget = new ConfigWidget(this);
    displayWidget = new DisplayWidget(this);
}

void MainWidget::createLayouts()
{
    QHBoxLayout *mainLayout = new QHBoxLayout();
    mainLayout->addWidget(configWidget);
    mainLayout->addWidget(displayWidget);
    mainLayout->setStretchFactor(configWidget, 1);
    mainLayout->setStretchFactor(displayWidget, 2);
    this->setLayout(mainLayout);
}


void MainWidget::createConnections()
{
    connect(configWidget, &ConfigWidget::setParameters, displayWidget, &DisplayWidget::parseParameters);
    connect(configWidget, &ConfigWidget::flushSignal, displayWidget, &DisplayWidget::flushDisplay);
    connect(configWidget, &ConfigWidget::sendInstruction, displayWidget, &DisplayWidget::receiveInstruction);
    connect(configWidget, &ConfigWidget::makeNextStep, displayWidget, &DisplayWidget::execNextStep);
    connect(displayWidget, &DisplayWidget::finishExec, configWidget, &ConfigWidget::onFinishSingleInstr);
    connect(configWidget, &ConfigWidget::resetInstrExecSignal, displayWidget, &DisplayWidget::resetExecution);
    connect(displayWidget, &DisplayWidget::cacheConfigIsNotSet, configWidget, &ConfigWidget::onCacheIsNotConfigured);
    connect(configWidget, &ConfigWidget::sendInstructions, displayWidget, &DisplayWidget::execAll);
    connect(configWidget, &ConfigWidget::sendInstrForwardExec, displayWidget, &DisplayWidget::execForward);
}


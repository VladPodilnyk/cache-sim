#include <QApplication>
#include <QFile>
#include <QTextStream>

#include "MainWidget.h"


int main (int argc, char * argv [])
{
    QApplication application (argc, argv);
    qRegisterMetaType<core::CacheParams>("CacheParams");
    qRegisterMetaType<core::InstrInfo>("InstrInfo");
    //qRegisterMetaType<std::vector <core::InstrInfo>>("InstrInfoList");
    MainWidget mainWidget;
    mainWidget.showMaximized ();
    
    return application.exec ();
}

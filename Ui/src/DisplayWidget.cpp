/**********************************************************
* file: DisplayWidget.cpp
* synopsis: Implementation for DisplayWidget class
* author: Vladyslav Podilnyk
**********************************************************/

#include <sstream>
#include <iomanip>

#include <QHeaderView>
#include <QGridLayout>
#include <QSizePolicy>
#include <QStandardItem>
#include <QString>
#include <QVariant>
#include <QFrame>
#include <QBrush>

#include "DisplayWidget.h"

DisplayWidget::DisplayWidget(QWidget *parent)
    : QWidget              {parent}
    , cacheBlocksTable     {nullptr}
    , cacheModel           {nullptr}
    , blockHeadLable       {nullptr}
    , offsetHeadLable      {nullptr}
    , blockLable           {nullptr}
    , offsetLable          {nullptr}
    , statsView            {nullptr}
    , hintView             {nullptr}
    , mainLayout           {new QVBoxLayout()}
    , lastHighlightedBlock {0}
    , cacheCtrl            {new core::CacheController()}
{
    createWidgets();
    createLayouts();
}


DisplayWidget::~DisplayWidget()
{
    delete cacheCtrl;
}


void DisplayWidget::createInstructionTable()
{
    blockHeadLable = new QLabel(tr("Block"), this);
    blockLable = new QLabel(this);
    offsetHeadLable = new QLabel(tr("Offset"), this);
    offsetLable = new QLabel(this);

    blockHeadLable->setFrameStyle(QFrame::Plain | QFrame::StyledPanel);
    blockLable->setFrameStyle(QFrame::Plain | QFrame::StyledPanel);
    offsetHeadLable->setFrameStyle(QFrame::Plain | QFrame::StyledPanel);
    offsetLable->setFrameStyle(QFrame::Raised | QFrame::StyledPanel);

    blockHeadLable->setAlignment(Qt::AlignCenter);
    blockLable->setAlignment(Qt::AlignCenter);
    offsetHeadLable->setAlignment(Qt::AlignCenter);
    offsetLable->setAlignment(Qt::AlignCenter);

    blockHeadLable->setStyleSheet("QLabel { background-color : white; }");
    offsetHeadLable->setStyleSheet("QLabel { background-color : white; }");
    blockLable->setStyleSheet("QLabel { background-color : white; }");
    offsetLable->setStyleSheet("QLabel { background-color : white; }");
}


void DisplayWidget::createCacheBlocksTable()
{
    cacheBlocksTable = new QTableView(this);
    cacheModel = new QStandardItemModel(this);

    cacheBlocksTable->verticalHeader()->setVisible(false);
    cacheModel->setHorizontalHeaderLabels ({tr("Index"), tr("Valid Bit"), 
                                            tr("Tag"),   tr("Data"), 
                                            tr("Dirty Bit")});
    cacheBlocksTable->setModel(cacheModel);
    cacheBlocksTable->horizontalHeader()->setSectionResizeMode(cacheTableIndexCol, 
                                                               QHeaderView::Stretch);
    cacheBlocksTable->horizontalHeader()->setSectionResizeMode(cacheTableValidCol, 
                                                               QHeaderView::Stretch);
    cacheBlocksTable->horizontalHeader()->setSectionResizeMode(cacheTableTagCol, 
                                                               QHeaderView::Stretch);
    cacheBlocksTable->horizontalHeader()->setSectionResizeMode(cacheTableDataCol, 
                                                               QHeaderView::Stretch);
    cacheBlocksTable->horizontalHeader()->setSectionResizeMode(cacheTableDirtyCol, 
                                                               QHeaderView::Stretch);
}


void DisplayWidget::createStatsAndHintViews()
{
    statsView = new QPlainTextEdit(this);
    hintView = new QPlainTextEdit(this);
}


void DisplayWidget::createWidgets()
{
    createInstructionTable();
    createCacheBlocksTable();
    createStatsAndHintViews();
}


void DisplayWidget::createInstructionPanel()
{
    QGridLayout *displayInstrGrid = new QGridLayout();
    displayInstrGrid->addWidget(blockHeadLable, 0, 0);
    displayInstrGrid->addWidget(blockLable, 1, 0);
    displayInstrGrid->addWidget(offsetHeadLable, 0, 1);
    displayInstrGrid->addWidget(offsetLable, 1, 1);
    displayInstrGrid->setHorizontalSpacing(1);
    displayInstrGrid->setVerticalSpacing(1);

    mainLayout->addWidget(new QLabel("Instruction Breakdown"));
    mainLayout->addLayout(displayInstrGrid);
}


void DisplayWidget::createCachePanel()
{
    mainLayout->addWidget(cacheBlocksTable);
    mainLayout->setStretchFactor(cacheBlocksTable, 2);
}


void DisplayWidget::createStatsAndHintPanel()
{
    QHBoxLayout *infoView = new QHBoxLayout();
    infoView->addWidget(statsView);
    infoView->addWidget(hintView);

    mainLayout->addLayout(infoView);
}


void DisplayWidget::createLayouts()
{
    createInstructionPanel();
    createCachePanel();
    createStatsAndHintPanel();
    this->setLayout(mainLayout);
}


void DisplayWidget::parseParameters(core::CacheParams &params)
{
    cacheCtrl->setConfig(&params);
    blockLable->setText(QString::number(cacheCtrl->getConfig().tag) + " bit");
    offsetLable->setText(QString::number(cacheCtrl->getConfig().params.offset) + " bit");

    cacheModel->removeRows(0, cacheModel->rowCount());

    for (auto &cacheLine: cacheCtrl->getData()) {
        QStandardItem *cellIndex = new QStandardItem();
        QStandardItem *cellIsValid = new QStandardItem();
        QStandardItem *cellTag = new QStandardItem();
        QStandardItem *cellData = new QStandardItem();
        QStandardItem *cellIsDirty = new QStandardItem();

        cellIndex->setTextAlignment(Qt::AlignCenter);
        cellIndex->setData(QVariant(static_cast <uint> (cacheLine.index)), Qt::DisplayRole);

        cellIsValid->setTextAlignment(Qt::AlignCenter);
        cellIsValid->setData(QVariant(static_cast <uint> (cacheLine.is_valid)), Qt::DisplayRole);

        cellTag->setTextAlignment(Qt::AlignCenter);
        cellTag->setData(QVariant(QString("-")), Qt::DisplayRole);

        cellData->setTextAlignment(Qt::AlignCenter);
        cellData->setData(QVariant(QString::fromUtf8(cacheLine.data.c_str())), Qt::DisplayRole);

        cellIsDirty->setTextAlignment(Qt::AlignCenter);
        cellIsDirty->setData(QVariant(static_cast <uint> (cacheLine.is_dirty)), Qt::DisplayRole);

        cacheModel->appendRow({cellIndex, cellIsValid, cellTag, cellData, cellIsDirty});
    }

    QString text = "Statistics\n\nHit time: " + QString::number(core::hit_time) + " cycles"
                                              + "\nMiss penalty: "
                                              + QString::number(core::miss_penalty) + " cycles";
    statsView->appendPlainText(text);
}


void DisplayWidget::flushDisplay()
{
    blockHeadLable->setText("Block");
    offsetHeadLable->setText("Offset");
    blockLable->clear();
    offsetLable->clear();
    cacheModel->removeRows(0, cacheCtrl->getConfig().cache_blocks_num);
    statsView->clear();
    hintView->clear();
}


std::string DisplayWidget::intToBinString(size_t number, size_t width)
{
    std::string result;
    for (ssize_t i = width - 1; i >= 0 ; --i) {
        size_t bit = number >> i;
        result += (bit & 1) ? "1" : "0";
    };

    return result;
}


void DisplayWidget::highlightCacheLine(size_t index, QColor color)
{
    for (int column = 0; column < cacheModel->columnCount(); ++column)
        cacheModel->item(index, column)->setData(QBrush(color), Qt::BackgroundRole);
}


QString DisplayWidget::returnStatsInText()
{
    core::CacheStats stats = cacheCtrl->getStatistics();
    double hitRate = static_cast <double> (stats.hits) / stats.total_instr * 100;
    double missRate = static_cast <double> (stats.misses) / stats.total_instr * 100;
    QString text = "Hit rate: " + QString::number(hitRate) + "%\n";
    text += "Miss rate: " + QString::number(missRate) + "%\n";
    text += "AMAT: " + QString::number(stats.amat) + "(cycles)\n";
    text += "Memory stalls: " + QString::number(stats.mem_stall_cycles) + "(cycles)\n";
    return text;
}


void DisplayWidget::receiveInstruction(core::InstrInfo &instr)
{
    if (cacheCtrl->getData().empty()) {
        emit cacheConfigIsNotSet();
        return;
    }

    instruction = instr;
    blockHeadLable->setStyleSheet("QLabel { background-color : white; }");
    blockHeadLable->setText(QString::fromUtf8(intToBinString(instruction.decoded_instr.tag, 
                                                             cacheCtrl->getConfig().tag).c_str()));
    offsetHeadLable->setText(QString::fromUtf8(intToBinString(instruction.decoded_instr.offset,
                                                              cacheCtrl->getConfig().params.offset).c_str()));
    highlightCacheLine(lastHighlightedBlock, Qt::white);

    hintView->clear();
    hintView->appendPlainText("The instruction has been loaded \
                               \nand allocated to block and offset \
                               \nrespectively");
}


void DisplayWidget::execNextStep()
{
    cacheBlockNumber = cacheCtrl->nextStep(instruction);

    if (cacheCtrl->getData().size() > cacheBlockNumber) {
        blockHeadLable->setStyleSheet("QLabel { background-color : white; }");
        core::CacheBlock cacheBlock = cacheCtrl->getBlock(cacheBlockNumber);

        QStandardItem *cellIndex = new QStandardItem();
        QStandardItem *cellIsValid = new QStandardItem();
        QStandardItem *cellTag = new QStandardItem();
        QStandardItem *cellData = new QStandardItem();
        QStandardItem *cellIsDirty = new QStandardItem();

        cellIndex->setTextAlignment(Qt::AlignCenter);
        cellIndex->setData(QVariant(static_cast <uint> (cacheBlock.index)), Qt::DisplayRole);

        cellIsValid->setTextAlignment(Qt::AlignCenter);
        cellIsValid->setData(QVariant(static_cast <uint> (cacheBlock.is_valid)), Qt::DisplayRole);

        cellTag->setTextAlignment(Qt::AlignCenter);
        cellTag->setData(QVariant(QString::fromUtf8(intToBinString(cacheBlock.tag, cacheCtrl->getConfig().tag).c_str())),
                                  Qt::DisplayRole);

        cellData->setTextAlignment(Qt::AlignCenter);
        cellData->setData(QVariant(QString::fromUtf8(cacheBlock.data.c_str())), Qt::DisplayRole);

        cellIsDirty->setTextAlignment(Qt::AlignCenter);
        cellIsDirty->setData(QVariant(static_cast <uint> (cacheBlock.is_dirty)), Qt::DisplayRole);

        cacheModel->insertRow(cacheBlockNumber,{cellIndex, cellIsValid, cellTag, cellData, cellIsDirty});
        cacheModel->removeRow(cacheBlockNumber + 1);

        // paint row
        if (cacheCtrl->getState() != core::CacheState::look_up) {
            isMissed = false;
            blockHeadLable->setStyleSheet("QLabel { background-color : lime; }");
            highlightCacheLine(cacheBlockNumber, Qt::green);
            hintView->clear();
            hintView->appendPlainText("Cache controller found a cache block \
                                       \nwith highlighted tag.");
        }
        else {
            highlightCacheLine(cacheBlockNumber, Qt::blue);
            updateDataExplaination();
        }

        // send singnal if cntrl finished execution
        if (cacheCtrl->getState() == core::CacheState::look_up) {
            lastHighlightedBlock = cacheBlockNumber;
            statsView->clear();
            statsView->appendPlainText(returnStatsInText());
            emit finishExec();
        }
    }
    else { // higlight rows according to cache look up
        blockHeadLable->setStyleSheet("QLabel { background-color : red; }");
        hintView->clear();
        hintView->appendPlainText("No cache block with given tag.");
        isMissed = true;
    }
}


void DisplayWidget::execAll(std::vector <core::InstrInfo> &instruction_queue)
{
    QString text = "Statistics\n\nHit time: " + QString::number(core::hit_time) + " cycles"
                                              + "\nMiss penalty: "
                                              + QString::number(core::miss_penalty) + " cycles\n\n";
    statsView->appendPlainText(text);
    cacheCtrl->fastForward(instruction_queue);
    statsView->clear();
    statsView->appendPlainText(text + returnStatsInText());
}


void DisplayWidget::execForward()
{
    lastHighlightedBlock = cacheCtrl->forwardInstr(instruction);
    updateDataExplaination();

    statsView->clear();
    statsView->appendPlainText(returnStatsInText());
    core::CacheBlock cacheBlock = cacheCtrl->getBlock(lastHighlightedBlock);
    
    QStandardItem *cellIndex = new QStandardItem();
    QStandardItem *cellIsValid = new QStandardItem();
    QStandardItem *cellTag = new QStandardItem();
    QStandardItem *cellData = new QStandardItem();
    QStandardItem *cellIsDirty = new QStandardItem();
    
    cellIndex->setTextAlignment(Qt::AlignCenter);
    cellIndex->setData(QVariant(static_cast <uint> (cacheBlock.index)), Qt::DisplayRole);
    
    cellIsValid->setTextAlignment(Qt::AlignCenter);
    cellIsValid->setData(QVariant(static_cast <uint> (cacheBlock.is_valid)), Qt::DisplayRole);
    
    cellTag->setTextAlignment(Qt::AlignCenter);
    cellTag->setData(QVariant(QString::fromUtf8(intToBinString(cacheBlock.tag, cacheCtrl->getConfig().tag).c_str())),
                                                Qt::DisplayRole);
    
    cellData->setTextAlignment(Qt::AlignCenter);
    cellData->setData(QVariant(QString::fromUtf8(cacheBlock.data.c_str())), Qt::DisplayRole);
    
    cellIsDirty->setTextAlignment(Qt::AlignCenter);
    cellIsDirty->setData(QVariant(static_cast <uint> (cacheBlock.is_dirty)), Qt::DisplayRole);
    
    cacheModel->insertRow(lastHighlightedBlock,{cellIndex, cellIsValid, cellTag, cellData, cellIsDirty});
    cacheModel->removeRow(lastHighlightedBlock + 1);

    highlightCacheLine(lastHighlightedBlock, Qt::blue);
    emit finishExec();
}


void DisplayWidget::resetExecution()
{
    flushDisplay();
    blockLable->setText(QString::number(cacheCtrl->getConfig().tag) + " bit");
    offsetLable->setText(QString::number(cacheCtrl->getConfig().params.offset) + " bit");

    QString text = "Statistics\n\nHit time: " + QString::number(core::hit_time) + " cycles"
                                              + "\nMiss penalty: "
                                              + QString::number(core::miss_penalty) + " cycles";
    statsView->appendPlainText(text);
    cacheCtrl->reset();

    for (auto &cacheLine: cacheCtrl->getData()) {
        QStandardItem *cellIndex = new QStandardItem();
        QStandardItem *cellIsValid = new QStandardItem();
        QStandardItem *cellTag = new QStandardItem();
        QStandardItem *cellData = new QStandardItem();
        QStandardItem *cellIsDirty = new QStandardItem();

        cellIndex->setTextAlignment(Qt::AlignCenter);
        cellIndex->setData(QVariant(static_cast <uint> (cacheLine.index)), Qt::DisplayRole);

        cellIsValid->setTextAlignment(Qt::AlignCenter);
        cellIsValid->setData(QVariant(cacheLine.is_valid), Qt::DisplayRole);

        cellTag->setTextAlignment(Qt::AlignCenter);
        cellTag->setData(QVariant(QString("-")), Qt::DisplayRole);

        cellData->setTextAlignment(Qt::AlignCenter);
        cellData->setData(QVariant(QString::fromUtf8(cacheLine.data.c_str())), Qt::DisplayRole);

        cellIsDirty->setTextAlignment(Qt::AlignCenter);
        cellIsDirty->setData(QVariant(cacheLine.is_dirty), Qt::DisplayRole);

        cacheModel->appendRow({cellIndex, cellIsValid, cellTag, cellData, cellIsDirty});
    }
}

void DisplayWidget::updateDataExplaination()
{
    hintView->clear();
    if (instruction.type == core::InstrType::load) {
        if (!isMissed)
            hintView->appendPlainText("Cache load data from the cache.");
        else {
            if (cacheCtrl->getData().size() == cacheCtrl->getNumberOfAllocBlocks()) 
                if (cacheCtrl->getConfig().params.rp_policy == core::ReplacementPolicy::lru)
                        hintView->appendPlainText("Load new memory block instead of \
                                                   the least recently used cache block.");
                else
                    hintView->appendPlainText("Load new memory block instead of\n"
                                               + QString::number(cacheBlockNumber)
                                               + "cache block according to fifo policy.");

            else
                hintView->appendPlainText("Load new memory block to the cache.");
        }
    }
    else {
        if (!isMissed) {
            if (cacheCtrl->getConfig().params.wr_policy.first_policy == core::WritePolicy::write_back)
                hintView->appendPlainText("Write new data to the cache block and mark \
                                           appropriate block with a dirty bit.");
            else
                hintView->appendPlainText("Write new data to the cache and the memory.\n");
        }
        else {
            if (cacheCtrl->getConfig().params.wr_policy.second_policy == core::WritePolicy::write_on_alloc)
                hintView->appendPlainText("Write new data to the memory \
                                           and load it to the cache using \
                                           current replacement policy.");
            else
                hintView->appendPlainText("Write new data to the memory block \
                                           with number " + QString::number(instruction.decoded_instr.tag));
        }
    }
}
/**********************************************************
* file: ConfigValidator.h
* synopsis: API for ConfigValidator class
* author: Vladyslav Podilnyk
**********************************************************/

#ifndef CACHE_SIM_CONFIG_VALIDATOR_H
#define CACHE_SIM_CONFIG_VALIDATOR_H

#include <cmath>
#include <QValidator>
#include <QMessageBox>
#include <QString>

#include <types.h>

class ConfigValidator
{
    public:

        static QValidator::State validate(size_t memorySize, size_t cacheSize, size_t offset)
        {
            if ((std::ceil(std::log2(memorySize)) == std::floor(std::log2(memorySize)))
                && (std::ceil(std::log2(cacheSize)) == std::floor(std::log2(cacheSize))) )

                if (memorySize > (cacheSize + static_cast <size_t> (std::pow(2,offset))) )
                    return QValidator::State::Acceptable;

            return QValidator::State::Invalid;
        };

        static void message(const QString &msg, core::ValidatorFlags flag)
        {
            QMessageBox message;
            message.setText(msg);

            if (flag == core::ValidatorFlags::error)
                message.setIcon(QMessageBox::Icon::Critical);
            else 
                message.setIcon(QMessageBox::Icon::Information);
            
            message.addButton(QMessageBox::Ok);
            message.setDefaultButton(QMessageBox::Ok);
            message.exec();
        };
};

#endif // CACHE_SIM_CONFIG_VALIDATOR_H
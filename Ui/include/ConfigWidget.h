/**********************************************************
* file: ConfigWidget.h
* synopsis: API for ConfigWidget class
* author: Vladyslav Podilnyk
**********************************************************/

#ifndef CACHE_SIM_CONFIG_WIDGET_H
#define CACHE_SIM_CONFIG_WIDGET_H

#include <string>
#include <vector>

#include <QWidget>
#include <QRadioButton>
#include <QPushButton>
#include <QButtonGroup>
#include <QLabel>
#include <QCheckBox>
#include <QLineEdit>
#include <QMessageBox>
#include <QDoubleSpinBox>
#include <QGroupBox>
#include <QVBoxLayout>

#include "types.h"
#include "InstructionGen.h"

class ConfigWidget : public QWidget
{
    Q_OBJECT

    public:
        explicit ConfigWidget(QWidget *parent = nullptr);
        ~ConfigWidget() override;

    signals:
        // signals 
        void setParameters(core::CacheParams&);
        void flushSignal();
        void makeNextStep();
        void resetInstrExecSignal();
        void sendInstruction(core::InstrInfo&);
        void sendInstrForwardExec();
        void sendInstructions(std::vector <core::InstrInfo>&);
    
    public slots:
        // slots
        void onFinishSingleInstr() { nextStep = false; isForward = false; }
        void onFinishSimulation() { runAllInstr = false; }
        void onCacheIsNotConfigured();

    private slots:
        void onSubmitButtonClick();
        void onResetButtonClick();
        void onStepByStepButtonClick();
        void onGenerateInsructionClick();
        void onNextStepButtonClick();
        void onResetInstrExecButtonClick();
        void onForwardInstrButtonClick();
        void onForwardAllButtonClick();
    
    private:
        // functions
        void createWritePolicyRadioButtons();
        void createReplacementPolicyRadioButtons();
        void createInstrTypeRadioButtons();
        void createControlElements();
        void createLineEdits();
        void createWritePolicyPanel();
        void createReplacementPolicyPanel();
        void createCacheConfigPanel();
        void createInstructionForm();
        void createExecutionControlPanel();
        void createWidgets();
        void createLayouts();
        void createConnections();
        std::string intToHexString(size_t /*number*/);

        // data
        QRadioButton   *writeBackPolicyCheck;
        QRadioButton   *writeOnAllocPolicyCheck;
        QRadioButton   *writeThroughPolicyCheck;
        QRadioButton   *writeAroundPolicyCheck;
        QRadioButton   *lruCacheCheck;
        QRadioButton   *fifoCacheCheck;
        QRadioButton   *storeInstrCheck;
        QRadioButton   *loadInstrCheck;
        QButtonGroup   *instrButtonGroup;
        QPushButton    *resetCacheConfigButton;
        QPushButton    *submitCacheConfigButton;
        QPushButton    *generateInstructionsButton;
        QPushButton    *nextStepButton;
        QPushButton    *stepByStepButton;
        QPushButton    *forwardInstrButton;
        QPushButton    *forwardAllButton;
        QPushButton    *flushInstrQueue;
        QPushButton    *resetInstrExec;
        QMessageBox    *controllerInfoBox;  // maybe TextEdit is appropriate for the case
        QLineEdit      *cacheSizeEdit;
        QLineEdit      *memorySizeEdit;
        QLineEdit      *offsetBitsEdit;
        QLineEdit      *instrNumberEdit;
        QLineEdit      *currentInstrEdit;
        QDoubleSpinBox *loadInstrProbability; // replace with SpinBox
        QGroupBox      *instrTypePanel;
        QVBoxLayout    *mainLayout;

        bool                  nextStep;
        bool                  isForward;
        bool                  runAllInstr;
        bool                  isInstrQueueEmpty;
        bool                  isConfigSet;
        core::InstructionGen  *instrGenerator;

        static constexpr int    bigSpacingValue   {20};
        static constexpr int    smallSpacingValue {5};
        static constexpr double probabilityStep   {0.05};
};

#endif // CACHE_SIM_CONFIG_WIDGET_H
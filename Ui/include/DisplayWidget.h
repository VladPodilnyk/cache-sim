/**********************************************************
* file: DisplayWidget.h
* synopsis: API for DisplayWidget class
* author: Vladyslav Podilnyk
**********************************************************/

#ifndef CACHE_SIM_DISPLAY_WIDGET_H
#define CACHE_SIM_DISPLAY_WIDGET_H

#include <string>
#include <vector>
#include <QWidget>
#include <QLabel>
#include <QColor>
#include <QTableView>
#include <QPlainTextEdit>
#include <QStandardItemModel>
#include <QVBoxLayout>

#include "CacheController.h"


class DisplayWidget : public QWidget
{
    Q_OBJECT

    public:
        explicit DisplayWidget(QWidget *parent = nullptr);
        ~DisplayWidget() override;
    
    signals:
        // signals
        void finishExec();
        void cacheConfigIsNotSet();

    public slots:
        // slots
        void parseParameters(core::CacheParams&);
        void receiveInstruction(core::InstrInfo&);
        void resetExecution();
        void execNextStep();
        void execForward();
        void execAll(std::vector <core::InstrInfo>&);
        void flushDisplay();

    private:
        // functions
        void createInstructionTable();
        void createCacheBlocksTable();
        void createStatsAndHintViews();
        void createInstructionPanel();
        void createCachePanel();
        void createStatsAndHintPanel();
        void createWidgets();
        void createLayouts();
        std::string intToBinString(size_t /*number*/, size_t /*width*/);
        void highlightCacheLine(size_t /*cache line index*/, QColor /*color*/);
        QString returnStatsInText();
        void updateDataExplaination();

        // data
        QTableView         *cacheBlocksTable;
        QStandardItemModel *cacheModel;
        QLabel             *blockHeadLable;
        QLabel             *offsetHeadLable;
        QLabel             *blockLable;
        QLabel             *offsetLable;
        QPlainTextEdit     *statsView;
        QPlainTextEdit     *hintView;
        QVBoxLayout        *mainLayout;

        bool                  isMissed;
        size_t                cacheBlockNumber;
        ssize_t               lastHighlightedBlock;
        core::InstrInfo       instruction;
        core::CacheController *cacheCtrl;

        static constexpr size_t instrTableColNum     {2};
        static constexpr size_t instrTableBlockCol   {0};
        static constexpr size_t instrTableOffsetCol  {1};
        static constexpr size_t cacheTableIndexCol   {0};
        static constexpr size_t cacheTableTagCol     {2};
        static constexpr size_t cacheTableValidCol   {1};
        static constexpr size_t cacheTableDataCol    {3};
        static constexpr size_t cacheTableDirtyCol   {4};
};

#endif // CACHE_SIM_DISPLAY_WIDGET_H
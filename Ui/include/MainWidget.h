/**********************************************************
* file: MainWidget.h
* synopsis: API for MainWidget class
* author: Vladyslav Podilnyk
**********************************************************/

#ifndef CACHE_SIM_MAIN_WIDGET_H
#define CACHE_SIM_MAIN_WIDGET_H

#include <QWidget>

#include "ConfigWidget.h"
#include "DisplayWidget.h"

Q_DECLARE_METATYPE(core::CacheParams)
Q_DECLARE_METATYPE(core::InstrInfo)
//Q_DECLARE_METATYPE(std::vector <core::InstrInfo>)

class MainWidget : public QWidget
{
    Q_OBJECT

    public:
        explicit MainWidget(QWidget *parent = nullptr);
        ~MainWidget() override = default;

    private:
        void createWidgets();
        void createLayouts();
        void createConnections();

        ConfigWidget *configWidget;
        DisplayWidget *displayWidget;
};

#endif // CACHE_SIM_MAIN_WIDGET_H
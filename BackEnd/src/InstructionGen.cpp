/**********************************************************
* file: InstructionGen.cpp
* synopsis: Implementation for InstructionGen class
* author: Vladyslav Podilnyk
**********************************************************/

#include <cmath>
#include <stdexcept>
#include <random>

#include "InstructionGen.h"

#include <iostream>

namespace core
{

void InstructionGen::setConfig(size_t memory_size, size_t new_offset)
{
    offset = new_offset; 
    mem_size = memory_size;
    curr_index = 0;
    instr_queue.clear();
}


InstrInfo InstructionGen::getInstr()
{
    if (curr_index == instr_queue.size())
        throw std::out_of_range("Index out of range");

    InstrInfo info = instr_queue[curr_index];
    curr_index = (curr_index++ > instr_queue.size()) ? 0 : curr_index;
    return info;
}


InstrStructure InstructionGen::instrBreakdown(size_t instr)
{
    InstrStructure cache_instr = { .tag = instr >> offset,
                                   .offset = instr & static_cast <int> ((std::pow(2, offset) - 1))
                                 };
    return cache_instr;
}

// TODO: use bernoulli distribution for genarating instructuion type
//      
void InstructionGen::generate(size_t instr_number, bool execute_all, double probability)
{
    std::random_device rd;
    std::mt19937 generator(rd());

    if (execute_all) {
        std::bernoulli_distribution bern_dist(probability);
        std::normal_distribution <> norm_dist{0, mem_size / 6};

        for (size_t i = 0; i < instr_number; ++i) {
            InstrInfo info;
            info.type = (bern_dist(generator)) ? InstrType::load : InstrType::store;
            auto rand_number = norm_dist(generator);
            info.instr = (rand_number < 0 ) ? rand_number * -1 : rand_number;
            info.decoded_instr = instrBreakdown(info.instr);
            instr_queue.push_back(info);
        }
    }
    else {
        #if 0
        std::vector <size_t> instructions = {0x428, 0x0bb, 0x55d, 0x709, 0x55d, 0x428, 0x428};
        for (const auto &curr_instr: instructions) {
            InstrInfo info;
            info.instr = curr_instr;
            info.decoded_instr = instrBreakdown(curr_instr);
            instr_queue.push_back(info);
        }

        #else
        // perform mem_sze trials (means instruction in our case) with probability = 1/2
        // TODO: remove hardcoded value
        //std::binomial_distribution <> binom_dist(mem_size, 0.);

        std::normal_distribution <> norm_dist{0, mem_size / 6};

        std::uniform_int_distribution <> dist(0, mem_size);

        instr_queue.reserve(instr_number);
        for (size_t i = 0; i < instr_number; ++i) {
            InstrInfo info;
            auto rand_number = norm_dist(generator);
            info.instr = (rand_number < 0 ) ? rand_number * -1 : rand_number;
            //info.instr = dist(generator);
            info.decoded_instr = instrBreakdown(info.instr);
            instr_queue.push_back(info);
        }
        #endif
    }

}


void InstructionGen::reset()
{
    instr_queue.clear();
    curr_index = 0;
}

} // core

/**********************************************************
* file: CacheController.cpp
* synopsis: Implementation for CacheController class
* author: Vladyslav Podilnyk
**********************************************************/

#include <cmath>
#include <iostream>
#include "CacheController.h"

namespace core
{

void CacheController::setConfig(CacheParams *params)
{
    // setting up writing and replacement policies
    config.params.wr_policy.first_policy = params->wr_policy.first_policy;
    config.params.wr_policy.second_policy = params->wr_policy.second_policy;
    config.params.rp_policy = params->rp_policy;

    // setting up other parameters
    config.params.memory_size = params->memory_size;
    config.params.cache_size = params->cache_size;
    config.params.offset = params->offset;

    // calculate confuguration
    config.instr_len = static_cast <size_t> (std::log2(config.params.memory_size));
    config.cache_blocks_num = static_cast <size_t> (std::log2(config.params.cache_size));
    config.tag = config.instr_len - config.params.offset;
    config.memory_blocks_num = static_cast <size_t> (std::pow(2, config.tag));

    // init cache
    allocated_blocks = -1;
    cache.ordered_data.clear();
    cache.cache_ref.clear();
    cache.data.clear();

    cache.ordered_data.reserve(config.cache_blocks_num);
    for (size_t i = 0; i < config.cache_blocks_num; ++i) {
        CacheBlock new_block = { .index = i,
                                 .is_valid = false,
                                 .tag = static_cast <size_t> (std::pow(2, config.memory_blocks_num + 1)),
                                 .data = "empty",
                                 .is_dirty = false
                               };
        cache.ordered_data.push_back(new_block);
    }

    state = CacheState::look_up;
    stats.hits = 0;
    stats.misses = 0;
    stats.total_instr = 0;
}


void CacheController::fastForward(const std::vector <InstrInfo> &instruction_queue)
{
    for (const auto &instruction: instruction_queue) {
        nextStep(instruction); // look up data in cache
        nextStep(instruction); // update data according to write/replacement policies
    }
}


size_t CacheController::forwardInstr(const InstrInfo &instruction)
{
    nextStep(instruction);
    return nextStep(instruction);
}


size_t CacheController::nextStep(const InstrInfo &instruction)
{
    size_t cache_block_number;

    if (state == CacheState::look_up) {
        cache_block_number = cacheLookUp(instruction.decoded_instr.tag);
        stats.total_instr++;
        if (cache_block_number < cache.ordered_data.size())
            stats.hits++;
        else
            stats.misses++;

        stats.mem_stall_cycles = stats.misses * miss_penalty;
        stats.amat = hit_time + (static_cast <double> (stats.misses) / stats.total_instr) * miss_penalty;
        state = CacheState::update;
    }
    else {
        cache_block_number = updateData(instruction.type, instruction.decoded_instr.tag);
        state = CacheState::look_up;
    }

    return cache_block_number;
}


size_t CacheController::cacheLookUp(size_t tag)
{
    for (size_t i = 0; i < cache.ordered_data.size(); ++i)
        if (cache.ordered_data[i].tag == tag)
            return i;

    return cache.ordered_data.size() + 1;
}


size_t  CacheController::lruPolicy(size_t tag)
{
    size_t memory_block = allocated_blocks + 1;

    if (cache.cache_ref.find(tag) == cache.cache_ref.end()) {
        // cache miss
        if (cache.data.size() == config.cache_blocks_num) {
            // full cache
            memory_block = cache.data.back().index;
            cache.cache_ref.erase(cache.data.back().tag);
            cache.data.pop_back();

            if (cache.ordered_data[memory_block].is_dirty)
                cache.main_mem_block = tag;
        }
        else 
            //allocated_blocks = (++allocated_blocks >= cache.ordered_data.size()) ? 0 : allocated_blocks;//++allocated_blocks;
            ++allocated_blocks;

        CacheBlock new_block = { .index = memory_block,
                                 .is_valid = true,
                                 .tag = tag,
                                 .data = "Blk "
                                         + std::to_string(tag) 
                                         + " W:0-"
                                         + std::to_string(static_cast <int> (std::pow(2, config.params.offset) - 1)),
                                 .is_dirty = false
                               };
        cache.ordered_data[memory_block] = new_block;
        cache.data.push_front(new_block);
        cache.cache_ref.insert({tag, cache.data.begin()});

    } 
    else { // found in cache
        CacheIterator searched_data = cache.cache_ref[tag];
        cache.data.push_front(*searched_data);
        cache.data.erase(searched_data);
        cache.cache_ref[tag] = cache.data.begin();
        memory_block = searched_data->index;
    }

    return memory_block;
}


size_t CacheController::fifoPolicy(size_t tag)
{
    size_t memory_block = allocated_blocks + 1;

    if (cache.cache_ref.find(tag) == cache.cache_ref.end()) {
        // cache miss
        if (cache.data.size() == config.cache_blocks_num) {
            // full cache
            memory_block = cache.data.front().index;
            cache.cache_ref.erase(cache.data.front().tag);
            cache.data.pop_front();

            if (cache.ordered_data[memory_block].is_dirty)
                cache.main_mem_block = tag;
        }
        else 
            ++allocated_blocks;

        CacheBlock new_block = { .index = memory_block,
                                 .is_valid = true,
                                 .tag = tag,
                                 .data = std::to_string(tag) 
                                         + "W:0-"
                                         + std::to_string(std::pow(2, config.params.offset) - 1),
                                 .is_dirty = false
                               };
        cache.ordered_data[memory_block] = new_block;
        cache.data.push_back(new_block);
        cache.cache_ref.insert({tag, cache.data.begin()});
    } 
    else // found in cache
        memory_block = cache.cache_ref[tag]->index;

    return memory_block;
}


size_t CacheController::writeBack(size_t tag)
{
    CacheIterator searched_data = cache.cache_ref[tag];
    searched_data->is_dirty = true;
    cache.ordered_data[searched_data->index].is_dirty = true;

    // update usage according to policy
    if (config.params.rp_policy == ReplacementPolicy::lru)
        lruPolicy(tag);

    return searched_data->index;
}


size_t CacheController::writeThrough(size_t tag)
{
    cache.main_mem_block = tag;
    if (config.params.rp_policy == ReplacementPolicy::lru)
        lruPolicy(tag);

    return cache.cache_ref[tag]->index;
}


size_t CacheController::writeOnAllocate(size_t tag)
{
    cache.main_mem_block = tag;
    size_t updated_block;

    switch (config.params.rp_policy) {
        case ReplacementPolicy::lru: updated_block = lruPolicy(tag); break;
        case ReplacementPolicy::fifo: updated_block = fifoPolicy(tag); break;
    }

    return updated_block;
}


size_t CacheController::writeAround(size_t tag)
{
    cache.main_mem_block = tag;
    return tag;
}


size_t CacheController::updateData(InstrType instr_type, size_t tag)
{
    size_t updated_block;

    if (instr_type == load) {
        switch (config.params.rp_policy) {
            case ReplacementPolicy::lru: updated_block = lruPolicy(tag); break;
            case ReplacementPolicy::fifo: updated_block = fifoPolicy(tag); break;
        }
    }
    else
        if (cache.cache_ref.find(tag) == cache.cache_ref.end()) {
            // cahce miss
            if (config.params.wr_policy.second_policy == WritePolicy::write_around)
                updated_block = writeAround(tag);
            else
                updated_block = writeOnAllocate(tag);
        }
        else {
            // cache hit
            if (config.params.wr_policy.first_policy == WritePolicy::write_back)
                updated_block = writeBack(tag);
            else
                updated_block = writeThrough(tag);
        }

    return updated_block;
}


void CacheController::reset()
{
    allocated_blocks = -1;
    cache.ordered_data.clear();
    cache.cache_ref.clear();
    cache.data.clear();

    cache.ordered_data.reserve(config.cache_blocks_num);
    for (size_t i = 0; i < config.cache_blocks_num; ++i) {
        CacheBlock new_block = { .index = i,
                                 .is_valid = false,
                                 .tag = static_cast <size_t> (std::pow(2, config.memory_blocks_num + 1)),
                                 .data = "empty",
                                 .is_dirty = false
                               };
        cache.ordered_data.push_back(new_block);
    }

    state = CacheState::look_up;
    stats.hits = 0;
    stats.misses = 0;
    stats.total_instr = 0;
}

// DEBUG
void CacheController::print()
{
    std::cout << "Cache Layout" << std::endl;
    for (auto &cache_line: cache.ordered_data)
        std::cout << cache_line.index << " " << cache_line.is_valid << " "
                  << cache_line.tag << " " << cache_line.data  << " "
                  << cache_line.is_dirty  << std::endl;
}

} // core
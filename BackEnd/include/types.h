/**********************************************************
* file: types.h
* synopsis: useful data types
* author: Vladyslav Podilnyk
**********************************************************/

#ifndef CORE_TYPES_H
#define CORE_TYPES_H

#include <cstdlib>
#include <string>
#include <vector>
#include <unordered_map>
#include <list>

namespace core
{

enum WritePolicy       {write_back, write_on_alloc, write_through, write_around};
enum ReplacementPolicy {fifo, lru};
enum InstrType         {load, store};
enum CacheState        {look_up, update};
enum ValidatorFlags    {error, info};

constexpr size_t KiB                        {1024};
constexpr size_t MiB                        {KiB * KiB};

constexpr size_t max_main_memory_size       {32}; // GiB
constexpr size_t max_first_level_cache_size {64}; // KiB
constexpr size_t hit_time                   {1};  // The number of cycles needed to get data
                                                  //in case of cache hit.

constexpr size_t miss_penalty               {20}; // The number of cycles needed to get data
                                                  // from a main memory after cache miss.
                                                  // This is arbitrary number, but it should be
                                                  // choosed in more inteligent way
                                                  // that consider memory bus width.

struct InstrStructure
{
    size_t tag;
    size_t offset;
};


struct InstrInfo
{
    size_t         instr;
    InstrType      type;
    InstrStructure decoded_instr;
};


struct CacheParams
{
    struct CacheWritePolicy
    {
        WritePolicy       first_policy;   // write back or write through
        WritePolicy       second_policy;  // write on allocate or write around
    } wr_policy;

    ReplacementPolicy rp_policy;
    size_t            memory_size;
    size_t            cache_size;
    size_t            offset;
};


struct CacheBlock
{
    size_t      index;
    bool        is_valid;
    size_t      tag;
    std::string data;
    bool        is_dirty;
};


struct CacheConfig
{
    CacheParams params;
    size_t      instr_len;
    size_t      tag;
    size_t      cache_blocks_num;
    size_t      memory_blocks_num;
};


struct CacheStats
{
    size_t hits               {0};
    size_t misses             {0};
    size_t total_instr        {0};
    double amat               {0};   // average memory access time
    size_t mem_stall_cycles   {0};   // number of cycles during wich processor
                                     // is stalled waiting for memory access
};

using CacheIterator = std::list <CacheBlock>::iterator;

struct CacheType
{
    std::unordered_map <size_t, CacheIterator> cache_ref; 
    std::list <CacheBlock>                     data;
    std::vector <CacheBlock>                   ordered_data;
    size_t                                     main_mem_block; // last updated memory block
};

} // core

#endif // CORE_TYPES_H
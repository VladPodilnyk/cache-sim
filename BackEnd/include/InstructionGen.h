/**********************************************************
* file: InstructionGen.h
* synopsis: API for InstructionGen class
* author: Vladyslav Podilnyk
**********************************************************/

#ifndef CORE_INSTRUCTION_GEN_H
#define CORE_INSTRUCTION_GEN_H

#include <cstdlib>
#include <vector>

#include "types.h"

namespace core
{

class InstructionGen
{
    public:
        InstructionGen() = default;
        void setConfig(size_t /*memory size*/, size_t /*new offset*/);
        void generate(size_t /*number of instruction*/, bool /*execute all*/, double probability = 0);
        void reset();
        InstrInfo getInstr();
        InstrInfo getInstr(size_t index) { return instr_queue[index]; };
        std::vector <InstrInfo> getInstrList() { return instr_queue; };
        ~InstructionGen() = default;

    private:
        // functions
        InstrStructure instrBreakdown(size_t /*instruction*/);

        // data
        std::vector <InstrInfo> instr_queue;
        size_t                  offset;
        size_t                  curr_index;
        size_t                  mem_size;
};

} // core

#endif // CORE_INSTRUCTION_GEN_H
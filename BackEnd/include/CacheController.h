/**********************************************************
* file: CacheController.h
* synopsis: API for CacheController class
* author: Vladyslav Podilnyk
**********************************************************/

#ifndef CORE_CACHE_CONTROLLER_H
#define CORE_CACHE_CONTROLLER_H

#include "types.h"

namespace core
{

class CacheController
{
    public:
        CacheController() = default;
        void setConfig(CacheParams* /*cache parameters*/);
        CacheConfig getConfig() { return config; };
        CacheState getState() { return state; };
        CacheBlock getBlock(size_t index) { return cache.ordered_data[index]; };
        std::vector <CacheBlock> getData() { return cache.ordered_data; };
        CacheStats getStatistics() { return stats; };
        void reset();
        void fastForward(const std::vector <InstrInfo>& /*list of instructions*/);
        size_t forwardInstr(const InstrInfo& /*instruction*/);
        size_t nextStep(const InstrInfo& /*instruction*/);
        size_t cacheLookUp(size_t /*tag*/);
        size_t updateData(InstrType /*instruction type*/, size_t /*tag*/);
        size_t getNumberOfAllocBlocks() { return allocated_blocks + 1; };
        void print();
        ~CacheController() = default;

    private:
        // functions
        size_t lruPolicy(size_t /*tag*/);
        size_t fifoPolicy(size_t /*tag*/);

        size_t writeBack(size_t /*tag*/);
        size_t writeThrough(size_t /*tag*/);
        size_t writeOnAllocate(size_t /*tag*/);
        size_t writeAround(size_t /*tag*/);


        // data
        CacheType            cache;
        CacheConfig          config;
        CacheState           state;
        CacheStats           stats;
        size_t               allocated_blocks;
};

} // core

#endif // CORE_CACHE_CONTROLLER_H